/*  * \arg CreatedOn        : 03/Oct/2017
    * \arg LastModifiedOn   : 03/Oct/2017
    * \arg CreatededBy      : Nrsingh
    * \arg ModifiedBy       : Nrsingh
    * \arg Description      : Trigger for Account object.
*/
public with sharing class AccountTriggerHandler{
    SharingCreationAndDeletionHandler objHandler = new SharingCreationAndDeletionHandler();
    public AccountTriggerHandler() {
        // Constructor..
    }
    /***
        Name        : onAfterInsert
        Params      : newMapTransactions
        Description : Called on After Insert of Account records....
    */
    public void onAfterInsert(Map<Id, Account> newMapAccounts){
        objHandler.createShareRecords(null, newMapAccounts);
    }
    
    /***
        Name        : onAfterUpdate
        Params      : oldMapAccounts, newMapAccounts
        Description : Called on After Update of Account records....
    */
    public void onAfterUpdate(Map<Id, Account> oldMapAccounts, Map<Id, Account> newMapAccounts){
        objHandler.createShareRecords(oldMapAccounts, newMapAccounts);
    }
	public void onAfterUpdate(Map<Id, Account> oldMapAccounts, Map<Id, Account> newMapAccounts){
    }
}